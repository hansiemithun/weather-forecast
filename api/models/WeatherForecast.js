/**
 * WeatherForecast.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var http = require('http');

module.exports = {

  report: function (cityId, cb) {
    var url = 'http://api.openweathermap.org/data/2.5/forecast?id='+cityId+'&appid=24df9b641de5578f83bae9f5688fdb79';

    http.request(url, function (response) {
      var responseData = '';

      response.on('data', function (chunk) {
        responseData += chunk;
      });

      response.once('error', function (err) {
        res.serverError(err);
      });

      response.on('end', function () {
        try {
          cb(JSON.parse(responseData));
        } catch (e) {
          sails.log.warn('Could not parse response from options.hostname: ' + e);
        }
      });
    }).end();
  }

};

