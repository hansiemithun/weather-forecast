/**
 * WeatherForecastController
 *
 * @description :: Server-side logic for managing weatherforecasts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


var modules = {

  nth: function (d) {
    if (d > 3 && d < 21) return 'th'; // thanks kennebec
    switch (d % 10) {
      case 1:
        return "st";
      case 2:
        return "nd";
      case 3:
        return "rd";
      default:
        return "th";
    }
  },

  timeConverter: function (timestamp, hours) {
    var a = new Date(timestamp * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var day = days[a.getDay()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var nth = modules.nth(date);
    return hours ? day + ' ' + date + nth + ' ' + month + ' ' + year : hour + ':' + min;
  },

  convertKelvinToCelsius: function (kelvin) {
    var celsius = kelvin - 273;
    return parseFloat(Math.round(celsius * 100) / 100).toFixed(2);
  },

  report: function (req, res) {
    var selectedCity = req.param('city') || 6695236;
    var selectedTotDis = req.param('totDisplay') || 5;

    WeatherForecast.report(selectedCity, function (results) {
      var city = results.city;
      var cityId = city.id;
      var cityName = city.name;
      var country = city.country;
      var cityDetails = cityName + ', ' + country;
      var dates = {};
      var cities = [{id: 5128581, city: 'NewYork'},
        {id: 2643743, city: 'London'},
        {id: 6695236, city: 'Bangalore(Kanija Bhavan)'},
        {id: 1273294, city: 'Delhi'},
        {id: 1269843, city: 'Hyderabad'},
        {id: 1264527, city: 'Chennai'},
        {id: 1259229, city: 'Pune'}];
      var totalDisplays = [1, 2, 3, 4, 5, 6];

      for (var i = 0; i < results.list.length; i++) {
        var listItem = results.list[i];
        var timestamp = listItem.dt;
        var date = modules.timeConverter(timestamp, true);
        var hours = modules.timeConverter(timestamp, false);
        var celsius = modules.convertKelvinToCelsius(listItem.main.temp);

        if (dates[date]) {
          dates[date].push({hours, celsius});
        } else {
          dates[date] = [{hours, celsius}];
        }
      }
      res.view('homepage',
        {
          cityDetails, dates, selectedCity, selectedTotDis, cities, totalDisplays}
      );
    });
  }

};

module.exports = modules;
